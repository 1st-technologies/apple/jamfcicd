# JamfCICD

This project demonstrates how to deploy and manage your Jamf scripts using Gitlab CI/CD.

## Prerequisites

Readers of this document are assumed to have working knowledge of, Bash, Jamf, Apple Administration and Git.

* A Jamf Cloud Account or Local Jamf Pro Instance
* A Gitlab account or local instance

Install on both the Gitlab runner and development workstation.

### XMLLint

* To install on Redhat based distros, type `sudo yum install libxml2`
* To install on Debian based distros, type `sudo apt install libxml2-utils`
* To install on Windows, download and unzip [http://xmlsoft.org/sources/win32/libxml2-2.7.8.win32.zip](http://xmlsoft.org/sources/win32/libxml2-2.7.8.win32.zip) and copy the files from `bin` to a directory in the PATH.
* To install on Apple...
  * install HomeBrew Package manager by executing `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
  * Then install the XML command-line utilities package with the following command: `brew install xmlstarlet`

### ShellCheck

* To install on Redhat based distros, type:
  * `sudo yum -y install epel-release`
  * `sudo yum install ShellCheck`
* To install on Debian based distros, type `sudo apt install shellcheck`
* To install on Windows, download and unzip [https://github.com/koalaman/shellcheck/releases/download/v0.9.0/shellcheck-v0.9.0.zip](https://github.com/koalaman/shellcheck/releases/download/v0.9.0/shellcheck-v0.9.0.zip) and copy shellcheck.exe to a directory in the PATH.
* To install on Apple...
  * install HomeBrew Package manager by executing `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
  * Then install the XML command-line utilities package with the following command: `brew install shellcheck`

The development workstation must have an advanced Integration Development Environment (IDE) application such as [Microsoft Visual Studio Code](https://code.visualstudio.com/).

## How to use this Project

### Create a "stub" Jamf Script

1. Log in to Jamf Pro.
1. If necessary, create the required category.
   1. In the top-right corner of the page, click **Settings**.
   1. Click **Global Management**.
   1. Click **Categories**.
   1. Click **New**.
   1. Enter a **Display name** and choose a **priority** for the category where the **Display Name** matches the basename of the script. Avoid spaces or special characters.
   1. Click Save.
1. In the top-right corner of the page, click **Settings**.
1. Click **Computer Management**, **Scripts** then **New**.
1. Select the **General** pane and set **Display name** and **Category**.
1. Click the **Script** tab and enter `echo "Hello, World!"` in the script editor.
1. Click the **Options** tab and set the **Priority** and any required **Parameter Values**.
1. (Optional) Click the **Limitations** tab and configure operating system requirements for the script.
1. Click **Save**.
1. Note the script ID number from the URL in address bar.

### Configure a Gitlab Runner

This project employs a [Gitlab Runner](https://docs.gitlab.com/runner/) with the shell "executor".

1. If you have not already done so, create a Gitlab account and fork or download then push this repo to a new project.
1. Navigate to **Settings** > **CI/CD** and note the url and registration token.
   ![Fig. 1](images/1.png)
1. For Windows:
   1. Download the [Gitlab Runner for Windows x64](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe), copy it to C:\Gitlab-Runner and rename the file to "gitlab-runner.exe"
   1. Open a command prompt as admin, CD to C:\gitlab-runner and enter the following commands:
      * `gitlab-runner.exe install`
      * `git-lab-runner.exe start`
      * `gitlab-runner.exe register`
1. For Redhat or Redhat based distros:
   1. To download the installer, execute: `curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm"`
   1. Execute `rpm -i gitlab-runner_amd64.rpm` to install Gitlab Runner.
1. For Debian or Debian based Linux:
   1. To download the installer, execute: `curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"`
   1. Execute `dpkg -i gitlab-runner_amd64.deb` to install Gitlab Runner.
1. For Apple
   1. Download the appropriate installer:
      1. For Intel, execute `sudo curl --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64"`
      1. For Apple Silicon, execute `sudo curl --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-arm64"`
   1. Run `sudo chmod +x /usr/local/bin/gitlab-runner` to set execution permissions.
   1. To install the runner, execute `gitlab-runner install`.
   1. Execute `gitlab-runner start` to start the service.
1. When prompted, specify the URL noted above.:

   ```txt
   Enter the GitLab instance URL (for example, https://gitlab.com/):
   https://gitlab.com/
   ```

1. Specify the registration token noted above:

   ```txt
   Enter the registration token:
   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
   ```

1. Type `Worker Node` as a description.:

   ```txt
   Enter a description for the runner:
   [myhost]: Worker Node
   ```

1. Specify tags `training`:

   ```txt
   Enter tags for the runner (comma-separated):
   training
   ```

1. Specify a maintenance note for the runner. This could include the number of CPU cores, location or environment, e.g. "dev", "prod", uat", etc...

   ```txt
   Enter optional maintenance note for the runner:
   test
   ```

1. Specify `shell` for the "executor".:

   ```txt
   Enter an executor: parallels, shell, ssh, virtualbox, docker+machine, docker-ssh+machine, docker, docker-ssh, instance, kubernetes, custom, docker-windows:
   shell
   ```

   > **NOTE**: Tags are freeform labels you attach to your runners and ýou typically won't see this many to designate runner dependencies. See [The Docker executor](https://docs.gitlab.com/runner/executors/docker.html) for more information.
1. Ensure the "tags" section of all stages in the [.gitlab-ci.yml](.gitlab-ci.yml) file to reflect the tags specified above.
1. For Windows-based runners, either...
    1. Install [PowerShell 7](https://learn.microsoft.com/en-us/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.3) or...
    1. Change the `[[runners]]` > `shell` value in c:\Gitlab-Runner\config.toml from "pwsh" to "powershell" and restart the git lab runner service.

    ```ini
    ...
    [[runners]]
      name = "myRunner"
      url = "https://gitlab.com/"
      id = 00000000
      token = "xxxxxxxxxxxxxxxxxxxx"
      token_obtained_at = 1900-01-00T00:00:00Z
      token_expires_at = 0001-01-01T00:00:00Z
      executor = "shell"
      shell = "powershell"
    ...
    ```

| A Note on Gitlab Runner "Executors" |
| --- |
| Larger enterprises typically do not use the "shell" executor as this requires an additional layer of management to ensure that the runners themselves have all the required software. Instead, Gitlab runners tend to be configured with **Docker** executors. A Gitlab runner using the "Docker" executor will run all build stages in a pre-configured docker image that is built explicitly as a worker node with all required software and libraries. See [Docker executor](https://docs.gitlab.com/runner/executors/docker.html) docs for more information. |

### Create a Jamf API Account

1. Log in to Jamf Pro.
1. In the top-right corner of the page, click **Settings**.
1. Click **System Settings**.
1. Click **User Accounts & Groups.
1. Click **New**.
1. Select **Create Standard Account** and click **Next**.
1. On the **Account** pane, enter information about the account as needed.
1. In the **Privileges** pane, select READ for all catagories and check the boxes for **Create**, **Read**, and **Update** for **Scripts**
1. Click **Save**.
1. Generate the API key with the following command and save the output.
   `echo -n "USERID:PASSWORD" | base64`

Once you have these components in place, download or clone this project into your own repository. The project Name and "slug" must match the basename of the script. E.g. the project name and slug for the script `OfficePostInstall.sh` must be "**OfficePostInstall**".

Lastly, you must add the CI_JAMF_KEY variable to the project.

1. On the left sidebar, click on **Settings** and then select **CI/CD** from the dropdown menu.
1. Expand the **Variables** section.
1. Click on the **Add Variable** button.
1. In the **Key** field, type "CI_JAMF_KEY".
1. In the **Value** field, enter API key generated a above.
1. Check the box labeled **Mask variable**.
1. Click on the **Add Variable** button to save.

| A Note on Authentication |
| --- |
| Storing sensitive data in masked CI variables is not recommended for production environments. A more typical solution would leverage a central "secrets" manager such as [Hashicorp Vault](https://www.hashicorp.com/products/vault). See [Authenticating and reading secrets with HashiCorp Vault](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/) for more information. |

## GitLab CI/CD

The following describes how the [.gitlab-ci.yml](.gitlab-ci.yml) in this file works to automate the testing and deployment Jamf scripts.

Set up the initial variables, specifying your Jamf instance URL, Parameter list,and the script **ID** generated in the "[Create a Stub Jamf Script](#create-a-stub-jamf-script)" section above.

```yml
variables:
  SCRIPT_ID: [SCRIPT ID] # <-- Replace with script ID #
  SCRIPT_CATEGORY: "System" # <-- Set your desired script category
  SCRIPT_PARAMETERS: "Parameter 1 Label,Parameter 2 Label,..."
  JAMF_URL: "https://company.jamfcloud.com:443" # <-- Replace with your Jamf Pro URL
```

Declare stages

```yml
stages:
  - build
  - test
  - push
```

Defined the **build** job and specify the appropriate runner tags.

```yml
build-job:
  stage: build
  tags:
    - training
  script:
```

This script parses the SCRIPT_PARAMETERS variable and create the "parameters" XML node.

```bash
    - |
      paramCount=4

      if [ -z "$SCRIPT_PARAMETERS" ]; then
          scriptParameters="  <parameters />"
      else
          IFS=";"
          read -ra parametersArray <<< "$SCRIPT_PARAMETERS"
          scriptParameters+=$(printf "  <parameters>\n")
          for i in "${parametersArray[@]}"
          do
              scriptParameters+=$(printf "    <parameter${paramCount}>%s</parameter${paramCount}>\n" "$i")
              (( paramCount++ ))
          done
          scriptParameters+=$(printf "</parameters>\n")
      fi
```

Read the contents of the script and encode to base64 string.

```bash
      scriptContent=$(base64 < "${CI_PROJECT_DIR}/${CI_PROJECT_TITLE}.sh")
```

Build the the payload XML file.

```xml
      payloadContent=$(cat << EOF
      <?xml version="1.0" encoding="UTF-8"?>
      <script xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="script.xsd">
          <id>${SCRIPT_ID}</id>
          <name>${CI_PROJECT_TITLE}</name>
          <category>${SCRIPT_CATEGORY}</category>
          <filename>${CI_PROJECT_TITLE}.sh</filename>
          <info />
          <notes>
            ${CI_PROJECT_DESCRIPTION}
          </notes>
          <priority>Before</priority>
          $scriptParameters
          <os_requirements />
          <script_contents_encoded>
            $scriptContent
          </script_contents_encoded>
      </script>
      EOF
      )
      echo "$payloadContent" | tee "${CI_PROJECT_DIR}/payload.xml"
```

Set the stage execution rules to always run on every commit.

```yml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
      when: always
```

Defined the **test** job and specify the appropriate runner tags.

```yml
test-job:
  stage: test
  tags:
    - training
  script:
```

Test the bash script for errors using shellcheck and validate the XML file against the script.xsd schema file using xmllint. Then check the status of the Jamf instance using the "healthCheck" endpoint.

```bash
    - shellcheck -S error "${CI_PROJECT_DIR}/${CI_PROJECT_TITLE}.sh"
    - xmllint --schema "${CI_PROJECT_DIR}/script.xsd" "${CI_PROJECT_DIR}/payload.xml" --noout || (echo "XML Schema test failed" & exit 1)
    - |
      result=$(curl -sSL "${JAMF_URL}/healthCheck.html")
      if [ "$result" != "[]" ]; then
          printf "JamfCloud reported non-ready state: '%s'\nSee https://learn.jamf.com/bundle/jamf-pro-documentation-current/page/Jamf_Pro_Health_Check_Page.html\n" "$result"
          exit 1
      fi
```

Set the stage execution rules to always run on every commit.

```yml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
      when: always
```

Defined the **push** job and specify the appropriate runner tags.

```yml
push-job:
  stage: push
  tags:
    - $CI_PROJECT_ROOT_NAMESPACE
  script:
```

Echo out the contents of the resulting payload for debugging purposes, get an authentication token from the Jamf API and submit the payload using an HTML PUT.

```bash
    - |
      echo "=== DEBUG Payload ==="
      cat "${CI_PROJECT_DIR}/payload.xml"

      key="${CI_JAMF_KEY}"

      token=$(curl -sSL -X POST "${JAMF_URL}/api/v1/auth/token" --header "Authorization: Basic $key" | jq -r '.token')

      curl -L -X PUT "${JAMF_URL}/JSSResource/scripts/id/${SCRIPT_ID}" -H 'Accept: application/xml' -H 'Content-Type: application/xml' -H "Authorization: Bearer $token" -T "${CI_PROJECT_DIR}/payload.xml"
```

Set the stage execution rules to always run on only on commit to the default (i.e. "master") branch and only if there are changes to the script.

```yml
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    changes:
      - ${CI_PROJECT_TITLE}.sh
  ```

## Additional Reading

* [Jamf Pro User Accounts and Groups](https://docs.jamf.com/10.36.0/jamf-pro/install-guide-mac/Jamf_Pro_User_Accounts_and_Groups.html)
* [Jamf Pro API Overview](https://developer.jamf.com/jamf-pro/docs/jamf-pro-api-overview)
* [Jamf Privilege Requirements](https://developer.jamf.com/jamf-pro/docs/classic-api-minimum-required-privileges-and-endpoint-mapping)
* [Git primer](https://danielmiessler.com/study/git/)
